import sentencias as sql
import conector

def mostrar_registros():
    conn = conector.conexion()
    cursor = conn.cursor()
    try:
        cursor.execute(sql.sql_select)
        registros = cursor.fetchall()

        for registro in registros:
            print(registro)
    except Exception as e:
        print(f'Ocurrio un error en la transaccion: {e}')
    finally:
        conector.cerrar_conexion(conn, cursor)
        
def actualizar_datos(datos):
    conn = conector.conexion()
    cursor = conn.cursor()
    try:
        if len(datos) > 1:
            cursor.executemany(sql.sql_update, datos)
        else:
            cursor.execute(sql.sql_update, datos)
        conn.commit()
        reg_actualizados = cursor.rowcount
        print(f'Registros actualizados: {reg_actualizados}')
        mostrar_registros()
    except Exception as e:
        conn.rollback()
        print(f'Ocurrio un error en la transaccion: {e}')
    finally:
        conector.cerrar_conexion(conn, cursor)
    
def insertar_datos(datos):
    conn = conector.conexion()
    cursor = conn.cursor()
    try:
        if len(datos) > 1:
            cursor.executemany(sql.sql_insert, datos)
        else:
            cursor.execute(sql.sql_insert, datos)
        conn.commit()
        reg_insertados = cursor.rowcount
        print(f'Registros insertados: {reg_insertados}')
        mostrar_registros()
    except Exception as e:
        conn.rollback()
        print(f'Ocurrio un error en la transaccion: {e}')
    finally:
        conector.cerrar_conexion(conn, cursor)

def seleccionar_datos(valor):
    conn = conector.conexion()
    cursor = conn.cursor()
    try:
        if len(valor) == 1:
            cursor.execute(sql.sql_select_uno, valor)
            registro = cursor.fetchone()
        else:
            condicion = (valor,)
            cursor.execute(sql.sql_select_varios, condicion)
            registro = cursor.fetchall()
        print(registro)
    except Exception as e:
        print(f'Ocurrio un error en la transaccion: {e}')
    finally:
        conector.cerrar_conexion(conn, cursor)

def eliminar(valores):
    conn = conector.conexion()
    cursor = conn.cursor()
    try:
        if len(valores) > 1:
            condicion = (valores,)
            cursor.execute(sql.sql_eliminar_varios, condicion)
        else:
            cursor.execute(sql.sql_eliminar_uno, valores)
        conn.commit()
        reg_eliminados = cursor.rowcount
        print(f'Registros eliminados: {reg_eliminados}')
        mostrar_registros()
    except Exception as e:
        conn.rollback()
        print(f'Ocurrio un error en la transaccion: {e}')
    finally:
        conector.cerrar_conexion(conn, cursor)