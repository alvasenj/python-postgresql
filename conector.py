import psycopg2 as ps
import parametros as p

def conexion():
    conn = ps.connect(user=p.user,
                      password=p.password,
                      host=p.host,
                      port=p.port,
                      database=p.database)
    return conn

def cerrar_conexion(conn, cursor):
    conn.close()
    cursor.close()
