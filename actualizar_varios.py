import utils

datos_persona = input('Escribe los valores a actualizar para una persona (nombre, apellido, email) - pulsa 0 para salir: ')
valores = ()
while(datos_persona != '0'):
    persona = str(input('Escribe el id_persona a actualizar (numero): '))
    tupla =  tuple(datos_persona.split(', ')) + (persona,)
    valores += (tupla, ) 
    datos_persona = input('Escribe los valores a actualizar para una persona (nombre, apellido, email) - pulsa 0 para salir: ')

utils.actualizar_datos(valores)