sql_insert = 'INSERT INTO persona(nombre, apellido, email) VALUES(%s, %s, %s)'
sql_select = "SELECT * FROM persona"
sql_select_varios = "SELECT * FROM persona WHERE nombre IN %s"
sql_select_uno = "SELECT * FROM persona WHERE nombre LIKE  %s"
sql_update = "UPDATE persona SET nombre = %s, apellido = %s, email = %s WHERE id_persona = %s"
sql_eliminar_varios = "DELETE FROM persona WHERE nombre IN %s"
sql_eliminar_uno = "DELETE FROM persona WHERE nombre LIKE %s"